package me.sharkz.ultramention.storage;

import me.sharkz.milkalib.loaders.DatabaseLoader;
import me.sharkz.milkalib.storage.db.DataBase;
import me.sharkz.milkalib.storage.db.DataBaseSettings;
import me.sharkz.milkalib.storage.db.mysql.MySQLDataBase;
import me.sharkz.milkalib.storage.db.sqlite.SQLiteDataBase;
import me.sharkz.milkalib.storage.sql.Column;
import me.sharkz.milkalib.storage.sql.DataTypes;
import me.sharkz.milkalib.storage.sql.QueryBuilder;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.ultramention.UM;
import me.sharkz.ultramention.mention.Mention;
import me.sharkz.ultramention.mention.MentionManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class StorageManager {

    private final UM plugin;
    private final FileConfiguration config;
    private DataBase dataBase;
    private String PLAYERS_TABLE;
    private String IGNORED_TABLE;
    private String MENTIONS_TABLE;

    public StorageManager(UM plugin, FileConfiguration config) {
        this.plugin = plugin;
        this.config = config;
        getStorageMethod();
        if (dataBase == null) {
            plugin.disable("Cannot connect to database ! Check your configuration !");
            return;
        }
        dataBase.connect(connection -> {
            try {
                if (connection.isValid(config.getInt("storage.timeout", 5)))
                    MilkaLogger.success("Connected to database");
                else
                    plugin.disable("Could not connect to database !");
            } catch (SQLException throwables) {
                plugin.disable("Cannot connect to database : " + throwables.getMessage());
                throwables.printStackTrace();
            }

            // TABLES NAME
            String prefix = config.getString("storage.prefix");
            PLAYERS_TABLE = prefix + "players";
            IGNORED_TABLE = prefix + "ignored";
            MENTIONS_TABLE = prefix + "mentions";

            createDefaultTables();
        });
    }

    /**
     * Get storage type from FileConfiguration
     */
    private void getStorageMethod() {
        boolean useSQLITE = config.getString("storage-method", "sqlite").toLowerCase().equals("sqlite");
        DataBaseSettings dbSettings = new DatabaseLoader().load((YamlConfiguration) config, "storage");
        if (useSQLITE)
            dataBase = new SQLiteDataBase(plugin) {
                public DataBaseSettings getDataBaseSettings() {
                    return dbSettings;
                }
            };
        else
            dataBase = new MySQLDataBase(plugin) {
                @Override
                public DataBaseSettings getDataBaseSettings() {
                    return dbSettings;
                }
            };
    }

    /**
     * Create default tables
     */
    private void createDefaultTables() {
        // Players table
        dataBase.query(new QueryBuilder(PLAYERS_TABLE).createTableIfNotExists()
                .column(Column.dataType("player", DataTypes.VARCHAR))
                .column(Column.dataType("state", DataTypes.BOOLEAN))
                .primaryKey("player")
                .build());

        // Ignored table
        dataBase.query(new QueryBuilder(IGNORED_TABLE).createTableIfNotExists()
                .column(Column.dataType("player", DataTypes.VARCHAR))
                .column(Column.dataType("ignored", DataTypes.VARCHAR))
                .build());

        // Mentions table
        dataBase.query(new QueryBuilder(MENTIONS_TABLE).createTableIfNotExists()
                .column(Column.dataType("player", DataTypes.VARCHAR))
                .column(Column.dataType("type", DataTypes.VARCHAR))
                .column(Column.dataType("date", DataTypes.BIGINT))
                .primaryKey("date")
                .build());
    }

    public List<Mention> getMentions(MentionManager mentionManager) {
        List<Mention> mentions = new ArrayList<>();
        ResultSet rs = dataBase.executeQuery(new QueryBuilder(MENTIONS_TABLE).select().buildAllColumns());
        try {
            while (rs.next())
                mentions.add(new Mention(rs.getLong("date"),
                        mentionManager.getType(rs.getString("type")),
                        Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player")))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mentions;
    }

    public void insertMention(Mention mention) {
        dataBase.asyncQuery(new QueryBuilder(MENTIONS_TABLE).insert()
                .insert("player", mention.getPlayer().getUniqueId().toString())
                .insert("date", mention.getTime())
                .insert("type", mention.getMentionType().getKey())
                .build());
    }

    public void deleteMention(Mention mention) {
        dataBase.asyncQuery(new QueryBuilder(MENTIONS_TABLE).delete()
                .where("date", mention.getTime())
                .build());
    }

    /**
     * Check if player has mention enabled
     *
     * @param player to check
     * @return true if it is the case
     */
    public boolean hasMentionEnabled(OfflinePlayer player) {
        ResultSet rs = dataBase.executeQuery(new QueryBuilder(PLAYERS_TABLE).select()
                .allColumns()
                .where("player", player.getUniqueId().toString())
                .build());
        try {
            return rs.next() && rs.getBoolean("state");
        } catch (SQLException throwables) { }
        return false;
    }

    /**
     * Change mention state for player
     *
     * @param player to change
     * @param state  new state value
     */
    public void setMentionState(OfflinePlayer player, boolean state) {
        dataBase.asyncQuery(new QueryBuilder(PLAYERS_TABLE).update()
                .set("state", state)
                .toWhere()
                .where("player", player.getUniqueId().toString())
                .build());
    }

    /**
     * Insert player mention state into database
     *
     * @param player to initialize
     */
    public void initPlayer(OfflinePlayer player) {
        if (existsInPlayersTable(player)) return;
        dataBase.asyncQuery(new QueryBuilder(PLAYERS_TABLE).insert()
                .insert("player", player.getUniqueId().toString())
                .insert("state", true)
                .build());
    }

    /**
     * Check if player has been initialized
     *
     * @param player to check
     * @return true if it is the case
     */
    public boolean existsInPlayersTable(OfflinePlayer player) {
        ResultSet rs = dataBase.executeQuery(new QueryBuilder(PLAYERS_TABLE).select().allColumns().where("player", player.getUniqueId().toString()).build());
        try {
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Check if player ignore another player
     *
     * @param player  who ignore
     * @param ignored ignored
     * @return true if it's the case
     */
    public boolean isIgnoring(OfflinePlayer player, OfflinePlayer ignored) {
        ResultSet rs = dataBase.executeQuery(new QueryBuilder(IGNORED_TABLE).select()
                .allColumns()
                .where("player", player.getUniqueId().toString())
                .and("ignored", ignored.getUniqueId().toString())
                .build());
        try {
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Ignore a player
     *
     * @param player   who request
     * @param toIgnore who will be ignored
     */
    public void setIgnored(OfflinePlayer player, OfflinePlayer toIgnore) {
        if (isIgnoring(player, toIgnore)) return;
        dataBase.asyncQuery(new QueryBuilder(IGNORED_TABLE).insert()
                .insert("player", player.getUniqueId().toString())
                .insert("ignored", toIgnore.getUniqueId())
                .build());
    }

    /**
     * UnIgnore a player
     *
     * @param player  who request
     * @param ignored who was ignored
     */
    public void unIgnore(OfflinePlayer player, OfflinePlayer ignored) {
        dataBase.asyncQuery(new QueryBuilder(IGNORED_TABLE).delete()
                .where("player", player.getUniqueId().toString())
                .and("ignored", ignored.getUniqueId().toString())
                .build());
    }

    /**
     * Returns map of ignored players
     *
     * @return map of ignoring/ignored players
     */
    public Map<OfflinePlayer, OfflinePlayer> getIgnored() {
        Map<OfflinePlayer, OfflinePlayer> tmp = new HashMap<>();
        ResultSet rs = dataBase.executeQuery(new QueryBuilder(IGNORED_TABLE).select().buildAllColumns());
        try {
            while (rs.next())
                tmp.put(Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player"))),
                        Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("ignored"))));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tmp;
    }

    public Map<OfflinePlayer, Boolean> getPlayers() {
        Map<OfflinePlayer, Boolean> tmp = new HashMap<>();
        ResultSet rs = dataBase.executeQuery(new QueryBuilder(PLAYERS_TABLE).select().buildAllColumns());
        try {
            while (rs.next())
                tmp.put(Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player"))), rs.getBoolean("state"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tmp;
    }
}
