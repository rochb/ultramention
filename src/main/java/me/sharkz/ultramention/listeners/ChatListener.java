package me.sharkz.ultramention.listeners;

import me.sharkz.milkalib.utils.MUtils;
import me.sharkz.ultramention.mention.MentionManager;
import me.sharkz.ultramention.mention.MentionType;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ChatListener extends MUtils implements Listener {

    private final MentionManager mentionManager;

    public ChatListener(MentionManager mentionManager) {
        this.mentionManager = mentionManager;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        String message = e.getMessage();
        mentionManager.getTypes()
                .stream()
                .filter(mentionType -> (mentionType.getKey().equalsIgnoreCase("player")
                        && Bukkit.getOnlinePlayers()
                        .stream()
                        .anyMatch(player -> Pattern.compile(Pattern.quote(mentionType.getPrefix() + player.getName()), Pattern.CASE_INSENSITIVE)
                                .matcher(message)
                                .find()))
                        || Pattern.compile(Pattern.quote(mentionType.getPrefix().toLowerCase() + mentionType.getKey().toLowerCase()), Pattern.CASE_INSENSITIVE)
                        .matcher(message)
                        .find())
                .forEach(type -> mentionManager.mention(e.getPlayer(), type, e));
    }


}
