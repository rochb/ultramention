package me.sharkz.ultramention.listeners;

import me.sharkz.ultramention.players.PlayerManager;
import me.sharkz.ultramention.storage.StorageManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {

    private final PlayerManager playerManager;
    private final StorageManager storageManager;

    public PlayerListener(PlayerManager playerManager, StorageManager storageManager) {
        this.playerManager = playerManager;
        this.storageManager = storageManager;
    }

    @EventHandler
    private void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        if (!playerManager.getStates().containsKey(player))
            storageManager.initPlayer(player);
    }
}
