package me.sharkz.ultramention.players;

import com.google.common.collect.ImmutableMap;
import me.sharkz.milkalib.utils.MUtils;
import me.sharkz.ultramention.storage.StorageManager;
import me.sharkz.ultramention.translations.UMEN;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager extends MUtils {

    private Map<OfflinePlayer, OfflinePlayer> ignored;
    private Map<OfflinePlayer, Boolean> states;
    private final StorageManager storageManager;

    public PlayerManager(StorageManager storageManager) {
        this.storageManager = storageManager;
        load();
    }

    public void load(){
        states = new HashMap<>(storageManager.getPlayers());
        ignored = new HashMap<>(storageManager.getIgnored());
    }

    public boolean getState(OfflinePlayer player) {
        if (states.containsKey(player))
            return states.get(player);
        return storageManager.hasMentionEnabled(player);
    }

    public boolean toggleMention(OfflinePlayer player) {
        boolean state = getState(player);
        storageManager.setMentionState(player, !state);
        if (player.isOnline()) {
            if (state)
                message(player.getPlayer(), UMEN.MENTION_DISABLED.name(), UMEN.MENTION_DISABLED.toString());
            else
                message(player.getPlayer(), UMEN.MENTION_ENABLED.name(), UMEN.MENTION_ENABLED.toString());
        }
        return !state;
    }

    public boolean isIgnored(OfflinePlayer ignoring, OfflinePlayer ignored) {
        return storageManager.isIgnoring(ignoring, ignored);
    }

    public void toggleIgnore(OfflinePlayer player, OfflinePlayer ignored) {
        boolean ign = isIgnored(player, ignored);
        if (ign)
            storageManager.unIgnore(player, ignored);
        else
            storageManager.setIgnored(player, ignored);

        if (player.isOnline()) {
            if (ign)
                message(player.getPlayer(), UMEN.MENTION_UNIGNORED.name(), UMEN.MENTION_UNIGNORED.toString(), ImmutableMap.of("%player%", ignored.getName()));
            else
                message(player.getPlayer(), UMEN.MENTION_IGNORED.name(), UMEN.MENTION_IGNORED.toString(), ImmutableMap.of("%player%", ignored.getName()));
        }
    }

    public Map<OfflinePlayer, OfflinePlayer> getIgnored() {
        return ignored;
    }

    public Map<OfflinePlayer, Boolean> getStates() {
        return states;
    }
}
