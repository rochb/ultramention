package me.sharkz.ultramention.mention;

import org.bukkit.OfflinePlayer;

import java.util.Date;

public class Mention {

    private final long time;
    private final MentionType mentionType;
    private final OfflinePlayer player;

    public Mention(MentionType mentionType, OfflinePlayer player) {
        this.time = System.currentTimeMillis();
        this.mentionType = mentionType;
        this.player = player;
    }

    public Mention(long time, MentionType mentionType, OfflinePlayer player) {
        this.time = time;
        this.mentionType = mentionType;
        this.player = player;
    }

    public long getTime() {
        return time;
    }

    public MentionType getMentionType() {
        return mentionType;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }
}
