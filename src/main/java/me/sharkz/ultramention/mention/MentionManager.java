package me.sharkz.ultramention.mention;

import com.google.common.collect.ImmutableMap;
import me.sharkz.milkalib.hooks.PapiHook;
import me.sharkz.milkalib.utils.MUtils;
import me.sharkz.milkalib.utils.TimerBuilder;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.ultramention.loader.MentionTypeLoader;
import me.sharkz.ultramention.players.PlayerManager;
import me.sharkz.ultramention.storage.StorageManager;
import me.sharkz.ultramention.translations.UMEN;
import me.sharkz.ultramention.utils.UMUtil;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MentionManager extends MUtils {

    private List<MentionType> types = new ArrayList<>();
    private List<Mention> mentions = new ArrayList<>();
    private final PlayerManager playerManager;
    private final StorageManager storageManager;

    public MentionManager(PlayerManager playerManager, StorageManager storageManager) {
        this.playerManager = playerManager;
        this.storageManager = storageManager;
        registerPermissions();
        load();
        MilkaLogger.info(types.size() + " mentions loaded !");
    }

    public void load() {
        loadTypes();
        mentions = getMentions();
    }

    private List<Mention> getMentions() {
        return storageManager.getMentions(this);
    }

    public List<Mention> getPlayerMentions(OfflinePlayer player) {
        return mentions.stream().filter(mention -> mention.getPlayer().equals(player)).collect(Collectors.toList());
    }

    public void mention(Player player, MentionType type, AsyncPlayerChatEvent e) {
        if (player != e.getPlayer()
                || (type.getPermission() != null
                && !player.hasPermission(type.getPermission())))
            return;

        /* Cooldown */
        /* TODO : finish this shit
        Optional<Mention> lastMention = getPlayerMentions(player)
                .stream()
                .filter(mention -> mention.getMentionType().equals(type)
                        && ((mention.getTime() + mention.getMentionType().getDelay()) < System.currentTimeMillis()))
                .findFirst();

        if (lastMention.isPresent()) {
            message(player, UMEN.WAIT_BEFORE_MENTION.name(), UMEN.WAIT_BEFORE_MENTION.toString(), ImmutableMap.of("%time%",
                    TimerBuilder.getStringTime(((lastMention.get().getTime()/1000) + type.getDelay()) - (System.currentTimeMillis()/1000))));
            return;
            //(lastMention.get().getTime() - System.currentTimeMillis() * 1000)
        }*/

        String message = e.getMessage();
        AtomicReference<String> tmp = new AtomicReference<>();
        List<Player> toMention = new ArrayList<>();

        /* Players mention */
        Bukkit.getOnlinePlayers()
                .stream()
                .filter(m -> Pattern.compile(Pattern.quote(type.getPrefix() + m.getName()),
                        Pattern.CASE_INSENSITIVE)
                        .matcher(message)
                        .find())
                .forEach(m -> {
                    tmp.set(Pattern.compile(Pattern.quote(type.getPrefix() + m.getName()),
                            Pattern.CASE_INSENSITIVE)
                            .matcher(message)
                            .replaceAll(color(type.getFormat().replace("%player%", player.getName() + "&r"))));
                    toMention.add(m);
                });


        /* Others mention */
        Matcher matcher = Pattern.compile(Pattern.quote(type.getPrefix() + type.getKey()),
                Pattern.CASE_INSENSITIVE)
                .matcher(message);
        if (matcher.find()) {
            tmp.set(matcher.replaceAll(color(type.getFormat().replace("%player%", player.getName() + "&r"))));
            Bukkit.getOnlinePlayers()
                    .stream()
                    .filter(player1 -> type.getPermission() == null ||
                            player1.hasPermission(type.getPermission()))
                    .forEach(toMention::add);
        }

        /* Money */
        if (type.getPrice() > 0) {
            if (!UMUtil.takeMoney(player, type.getPrice())) {
                message(player, UMEN.NOT_ENOUGH_MONEY.name(), UMEN.NOT_ENOUGH_MONEY.toString());
                return;
            }
        }

        /* EXP */
        if (type.getXp() > 0) {
            if (!UMUtil.takeMoney(player, type.getXp())) {
                message(player, UMEN.NOT_ENOUGH_XP.name(), UMEN.NOT_ENOUGH_XP.toString());
                return;
            }
        }

        /* Placeholder API */
        if (PapiHook.isHooked())
            toMention.forEach(player1 -> tmp.set(UMUtil.setPlaceholders(player1, tmp.get())));

        /* Sound */
        if (type.getSound() != null && type.getSound().isSupported())
            toMention.forEach(player1 -> type.getSound().play(player1));

        toMention.forEach(player1 -> type.sendMessage(player, player1));

        addMention(new Mention(type, player));

        if (tmp.get() == null) return;
        e.setMessage(tmp.get());
    }

    private void loadTypes() {
        MentionTypeLoader loader = new MentionTypeLoader();
        types = getConfig().getConfigurationSection("mentions")
                .getKeys(true)
                .stream()
                .filter(s -> getConfig().isConfigurationSection("mentions." + s))
                .map(s -> loader.load((YamlConfiguration) getConfig(), "mentions." + s))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public void addMention(Mention mention) {
        storageManager.insertMention(mention);
        mentions.add(mention);
    }

    private void registerPermissions() {
        types.stream().filter(mentionType -> mentionType.getPermission() != null).forEach(mentionType -> registerPermission(mentionType.getPermission()));
    }

    public MentionType getType(String key) {
        return types.stream().filter(mentionType -> mentionType.getKey().equals(key)).findFirst().orElse(null);
    }

    public List<MentionType> getTypes() {
        return types;
    }
}
