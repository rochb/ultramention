package me.sharkz.ultramention.mention;

import me.sharkz.milkalib.utils.xseries.ActionBar;
import me.sharkz.milkalib.utils.xseries.Titles;
import me.sharkz.milkalib.utils.xseries.XSound;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;

public class MentionType {

    private final String prefix;
    private final String key;
    private @Nullable
    final String permission;
    private final String format;
    private final int delay;

    private @Nullable
    final String actionBar;

    private @Nullable
    final String chatMessage;

    private @Nullable
    final
    String title;
    private @Nullable
    final
    String subTitle;

    private
    final int price;

    private
    final int xp;

    private @Nullable
    final XSound sound;

    public MentionType(String prefix, String key, @Nullable String permission, String format, int delay, @Nullable String actionBar, @Nullable String chatMessage, @Nullable String title, @Nullable String subTitle, int price, int xp, @Nullable XSound sound) {
        this.prefix = prefix;
        this.key = key;
        this.permission = permission;
        this.format = format;
        this.delay = delay;
        this.actionBar = actionBar;
        this.chatMessage = chatMessage;
        this.title = title;
        this.subTitle = subTitle;
        this.price = price;
        this.xp = xp;
        this.sound = sound;
    }

    public void sendMessage(Player player, Player requester) {
        if (actionBar != null)
            ActionBar.sendActionBar(player, ChatColor.translateAlternateColorCodes('&', actionBar.replace("%player%", requester.getName())));
        if (chatMessage != null)
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', chatMessage.replace("%player%", requester.getName())));
        if (title != null && subTitle != null)
            Titles.sendTitle(player, ChatColor.translateAlternateColorCodes('&', title.replace("%player%", requester.getName())),
                    ChatColor.translateAlternateColorCodes('&', subTitle.replace("%player%", requester.getName())));
    }

    public String getPrefix() {
        return prefix;
    }

    public String getKey() {
        return key;
    }

    @Nullable
    public String getPermission() {
        return permission;
    }

    public String getFormat() {
        return format;
    }

    public int getDelay() {
        return delay;
    }

    @Nullable
    public String getActionBar() {
        return actionBar;
    }

    @Nullable
    public String getChatMessage() {
        return chatMessage;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getSubTitle() {
        return subTitle;
    }

    public int getPrice() {
        return price;
    }

    public int getXp() {
        return xp;
    }

    @Nullable
    public XSound getSound() {
        return sound;
    }
}
