package me.sharkz.ultramention.hooks;

import me.sharkz.milkalib.hooks.PluginHooker;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.ultramention.UM;
import org.bukkit.plugin.java.JavaPlugin;

public class DiscordHook implements PluginHooker {

    private static boolean hooked;

    @Override
    public String getRequiredPlugin() {
        return "DiscordSRV";
    }

    @Override
    public void hookSuccess() {
        MilkaLogger.success("Successfully hooked into DiscordSrv !");
        hooked = true;
    }

    @Override
    public void hookFailed() {
        hooked = false;
    }

    @Override
    public boolean disablePluginIfNotFound() {
        return false;
    }

    @Override
    public JavaPlugin getPlugin() {
        return UM.getInstance();
    }
}
