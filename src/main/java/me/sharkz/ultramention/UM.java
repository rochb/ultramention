package me.sharkz.ultramention;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.hooks.PapiHook;
import me.sharkz.milkalib.hooks.VaultHook;
import me.sharkz.milkalib.inventories.defaults.LanguageGUI;
import me.sharkz.milkalib.inventories.defaults.MiscellaneousGUI;
import me.sharkz.milkalib.inventories.defaults.StorageSettingsGUI;
import me.sharkz.milkalib.translations.TranslationsManager;
import me.sharkz.milkalib.translations.en.BaseEN;
import me.sharkz.milkalib.translations.fr.BaseFR;
import me.sharkz.milkalib.utils.ItemBuilder;
import me.sharkz.milkalib.utils.TimerBuilder;
import me.sharkz.milkalib.utils.xseries.XMaterial;
import me.sharkz.ultramention.commands.MainCommand;
import me.sharkz.ultramention.hooks.DiscordHook;
import me.sharkz.ultramention.inventories.MainGUI;
import me.sharkz.ultramention.listeners.ChatListener;
import me.sharkz.ultramention.listeners.PlayerListener;
import me.sharkz.ultramention.mention.MentionManager;
import me.sharkz.ultramention.players.PlayerManager;
import me.sharkz.ultramention.storage.StorageManager;
import me.sharkz.ultramention.translations.UMEN;
import org.bstats.bukkit.Metrics;
import org.bukkit.inventory.ItemStack;

import java.io.File;

public class UM extends MilkaPlugin {

    private MentionManager mentionManager;
    private PlayerManager playerManager;

    @Override
    public void onEnable() {
        /* Header */
        sendHeader("Sharkz");

        /* Dependencies */
        initDependencies();

        /* Configuration */
        loadConfiguration("version");

        /* Updates */
        if (getConfig().getBoolean("check-for-update", true))
            lookForUpdate();

        /* Metrics */
        new Metrics(this, 8762);

        /* Hooks */
        registerHook(new PapiHook());
        registerHook(new VaultHook());
        registerHook(new DiscordHook());

        /* Storage */
        StorageManager storageManager = new StorageManager(this, getConfig());

        /* Players */
        playerManager = new PlayerManager(storageManager);

        /* Mentions */
        mentionManager = new MentionManager(playerManager, storageManager);

        /* Translations */
        registerTranslation(BaseFR.class);
        registerTranslation(BaseEN.class);
        registerTranslation(UMEN.class);
        new TimerBuilder(getTranslationsManager());

        /* Commands */
        initCommands();
        registerCommand("ultramention", new MainCommand(playerManager), "um", "mention");

        /* Inventories */
        initInventories();
        registerInventory(1, new MainGUI());
        registerInventory(2, new LanguageGUI());
        registerInventory(3, new StorageSettingsGUI());
        registerInventory(4, new MiscellaneousGUI());

        /* Listeners */
        registerListener(new PlayerListener(playerManager, storageManager));
        registerListener(new ChatListener(mentionManager));

        /* Footer */
        sendFooter();
    }

    @Override
    public void onDisable() {
        /* Thanks */
        sendThanks();
    }

    @Override
    public void reload() {
        playerManager.load();
        mentionManager.load();

        super.reload();
    }

    @Override
    protected int getConfigurationVersion() {
        return 9;
    }

    @Override
    public String getSpigotMcLink() {
        return "https://www.spigotmc.org/resources/ultramention-1-8-1-16-fully-customizable.83567/";
    }

    @Override
    public int getResourceId() {
        return 83567;
    }

    @Override
    public String getPrefix() {
        return getConfig().getString("prefix", "&c&lULTRA&e&lMENTION &7|");
    }

    @Override
    public String getLanguage() {
        return getConfig().getString("language", "en_US");
    }

    @Override
    public TranslationsManager getTranslationsManager() {
        return new TranslationsManager(this);
    }

    @Override
    public File getTranslationsFolder() {
        return new File(getDataFolder(), "lang");
    }

    @Override
    public ItemStack getFiller() {
        return new ItemBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseMaterial())
                .setDurability(XMaterial.WHITE_STAINED_GLASS_PANE.getData())
                .setName("&r")
                .setLore("&7&m-------------------",
                        "&r",
                        getPrefix(),
                        "&7Developed by : &a&lSharkz",
                        "&7Version : &a&l" + getDescription().getVersion(),
                        "&r",
                        "&7&m-------------------").toItemStack();
    }
}
