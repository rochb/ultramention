package me.sharkz.ultramention.inventories;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.inventories.InventoryResult;
import me.sharkz.milkalib.inventories.MilkaInventory;
import me.sharkz.milkalib.utils.ItemBuilder;
import me.sharkz.milkalib.utils.SkullCreator;
import me.sharkz.milkalib.utils.xseries.XMaterial;
import org.bukkit.entity.Player;

public class MainGUI extends MilkaInventory {

    public MainGUI() {
        createInventory("", 9);
    }

    @Override
    public InventoryResult openInventory(MilkaPlugin milkaPlugin, Player player, int i, Object... objects) {
        addItem(1, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTI4OWQ1YjE3ODYyNmVhMjNkMGIwYzNkMmRmNWMwODVlODM3NTA1NmJmNjg1YjVlZDViYjQ3N2ZlODQ3MmQ5NCJ9fX0="))
                .setName("&b&lLanguage")
                .setLore("&7&m-------------------",
                        "&r",
                        "&bClick&7 to open language settings.",
                        "&r",
                        "&7&m-------------------"))
                .setClick(inventoryClickEvent -> openInventory(player, 2));

        addItem(3, new ItemBuilder(XMaterial.CHEST.parseMaterial())
                .setName("&d&lStorage")
                .setLore("&7&m-------------------",
                        "&r",
                        "&bClick&7 to open storage settings.",
                        "&r",
                        "&7&m-------------------"))
                .setClick(inventoryClickEvent -> openInventory(player, 3));

        addItem(5, new ItemBuilder(XMaterial.BUCKET.parseMaterial())
                .setName("&6&lMiscellaneous")
                .setLore("&7&m-------------------",
                        "&r",
                        "&bClick&7 to open miscellaneous settings.",
                        "&r",
                        "&7&m-------------------"))
                .setClick(inventoryClickEvent -> openInventory(player, 4));

        return InventoryResult.SUCCESS;
    }
}
