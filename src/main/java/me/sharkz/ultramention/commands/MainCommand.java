package me.sharkz.ultramention.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.commands.defaults.ReloadCommand;
import me.sharkz.milkalib.commands.defaults.VersionCommand;
import me.sharkz.ultramention.players.PlayerManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class MainCommand extends MilkaCommand {

    public MainCommand(PlayerManager playerManager) {
        setDescription("Plugin main command");
        setTabCompletor();
        addSubCommand(new HelpCommand());
        addSubCommand(new ReloadCommand("ultramention.reload"));
        addSubCommand(new VersionCommand());
        addSubCommand(new ToggleCommand(playerManager));
        addSubCommand(new SettingsCommand());
        addSubCommand(new IgnoreCommand(playerManager));
    }

    @Override
    protected CommandResult perform(MilkaPlugin milkaPlugin) {
        HelpCommand.sendHelpMenu(sender);
        return CommandResult.SUCCESS;
    }


    @Override
    public CommandResult getTabCompleter() {
        return CommandResult.SUCCESS;
    }

    @Override
    public List<String> toTab(MilkaPlugin plugin, CommandSender sender2, String[] args) {
        List<String> tmp = new ArrayList<>();
        if(args.length == 0){
            tmp.add("help");
            tmp.add("version");
            if(sender2.hasPermission("ultramention.reload")) tmp.add("reload");
            if(sender2.hasPermission("ultramention.toggle")) tmp.add("toggle");
            if(sender2.hasPermission("ultramention.ignore")) tmp.add("ignore");
            if(sender2.hasPermission("ultramention.settings")) tmp.add("settings");
        }
        return tmp;
    }
}
