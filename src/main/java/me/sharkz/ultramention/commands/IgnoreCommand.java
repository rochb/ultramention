package me.sharkz.ultramention.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.ultramention.players.PlayerManager;
import org.bukkit.entity.Player;

public class IgnoreCommand extends MilkaCommand {

    private final PlayerManager playerManager;

    public IgnoreCommand(PlayerManager playerManager) {
        this.playerManager = playerManager;
        setDescription("Ignore boring players.");
        addSubCommand("ignore", "block");
        setPermission("ultramention.ignore");
    }

    @Override
    protected CommandResult perform(MilkaPlugin milkaPlugin) {
        return CommandResult.SUCCESS;
    }
}
