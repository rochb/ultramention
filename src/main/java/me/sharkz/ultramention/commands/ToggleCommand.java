package me.sharkz.ultramention.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.ultramention.players.PlayerManager;

public class ToggleCommand extends MilkaCommand {

    private final PlayerManager playerManager;

    public ToggleCommand(PlayerManager playerManager) {
        this.playerManager = playerManager;
        addSubCommand("toggle", "enable", "disable", "on", "off");
        setDescription("Toggle your mention.");
        setPermission("ultramention.toggle");
        setConsoleCanUse(false);
    }

    @Override
    protected CommandResult perform(MilkaPlugin milkaPlugin) {
        playerManager.toggleMention(player);
        return CommandResult.SUCCESS;
    }
}
