package me.sharkz.ultramention.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import org.bukkit.command.CommandSender;

public class HelpCommand extends MilkaCommand {

    public HelpCommand() {
        addSubCommand("help", "h", "?");
        setDescription("Show help menu.");
    }

    @Override
    protected CommandResult perform(MilkaPlugin milkaPlugin) {
        sendHelpMenu(sender);
        return CommandResult.SUCCESS;
    }

    public static void sendHelpMenu(CommandSender sender){

    }
}
