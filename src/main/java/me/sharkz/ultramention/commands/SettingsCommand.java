package me.sharkz.ultramention.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

public class SettingsCommand extends MilkaCommand {

    public SettingsCommand() {
        setPermission("ultramention.settings");
        setDescription("Open settings gui.");
        setConsoleCanUse(false);
        addSubCommand("settings", "admin");
    }

    @Override
    protected CommandResult perform(MilkaPlugin milkaPlugin) {
        openInventory(player, 1);
        return CommandResult.SUCCESS;
    }
}
