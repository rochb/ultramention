package me.sharkz.ultramention.loader;

import me.sharkz.milkalib.loaders.Loader;
import me.sharkz.milkalib.utils.xseries.XSound;
import me.sharkz.ultramention.mention.MentionType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.concurrent.atomic.AtomicReference;

public class MentionTypeLoader implements Loader<MentionType> {

    @Override
    public MentionType load(YamlConfiguration config, String path) {
        ConfigurationSection sec = config.getConfigurationSection(path);
        assert sec != null;

        String prefix = sec.getString("prefix", "@");
        String key = sec.getName();
        String permission = sec.getString("permission");
        String format = sec.getString("format", "&a" + prefix + key + "&r");
        int delay = sec.getInt("delay");

        String actionBar = sec.getString("action-bar");
        String text = sec.getString("chat-message");
        String title = sec.getString("title.title");
        String subtitle = sec.getString("title.sub-title");
        int price = sec.getInt("price");
        int xp = sec.getInt("xp");
        AtomicReference<XSound> sound = new AtomicReference<>();
        String s = sec.getString("sound");
        if (s != null && !s.isEmpty())
            XSound.matchXSound(s).ifPresent(sound::set);

        return new MentionType(prefix, key, permission, format, delay, actionBar, text, title, subtitle, price, xp, sound.get());
    }

    @Override
    public void save(MentionType m, YamlConfiguration config, String path) {
        ConfigurationSection sec = config.createSection(path + "." + m.getKey());
        sec.set("prefix", m.getPrefix());
        sec.set("permission", m.getPermission());
        sec.set("format", ChatColor.stripColor(m.getPrefix()));
        sec.set("delay", m.getDelay());
        sec.set("action-bar", ChatColor.stripColor(m.getActionBar()));
        sec.set("chat-message", ChatColor.stripColor(m.getChatMessage()));
        sec.set("title.title", ChatColor.stripColor(m.getTitle()));
        sec.set("title.sub-title", ChatColor.stripColor(m.getSubTitle()));
        sec.set("price", m.getPrice());
        sec.set("xp", m.getXp());
        assert m.getSound() != null;
        sec.set("sound", m.getSound().name());
    }
}
