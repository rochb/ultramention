package me.sharkz.ultramention.utils;

import me.clip.placeholderapi.PlaceholderAPI;
import me.sharkz.milkalib.hooks.PapiHook;
import me.sharkz.milkalib.hooks.VaultHook;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class UMUtil {

    public static boolean takeMoney(OfflinePlayer player, double amount) {
        if (!VaultHook.isHooked() || VaultHook.getEconomy() == null) return false;
        Economy economy = VaultHook.getEconomy();
        if(economy.getBalance(player) < amount)
            return false;
        economy.withdrawPlayer(player, amount);
        return true;
    }

    public static String setPlaceholders(OfflinePlayer player, String message) {
        if (!PapiHook.isHooked()) return message;
        return PlaceholderAPI.setPlaceholders(player, message);
    }

    public static boolean takeExp(Player player, int levels) {
        if(player.getExpToLevel() < levels) return false;
        player.giveExpLevels(-levels);
        return true;
    }
}
