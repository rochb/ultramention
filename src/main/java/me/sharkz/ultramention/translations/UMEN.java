package me.sharkz.ultramention.translations;

import me.sharkz.milkalib.translations.Translation;
import me.sharkz.milkalib.translations.TranslationImpl;
import me.sharkz.milkalib.translations.TranslationsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public enum UMEN implements TranslationImpl {
    MENTION_ENABLED("&7You have &a&lenabled&7 your mention."),
    MENTION_DISABLED("&7You have &c&ldisabled&7 your mention."),
    MENTION_IGNORED("&7You now ignore the mentions coming from &c%player&7."),
    MENTION_UNIGNORED("&7You will now receive mentions from &a%player&7."),
    NOT_ENOUGH_MONEY("&cYou don't have enough money to mention this player !"),
    NOT_ENOUGH_XP("&cYou don't have enough experience level to mention this player !"),
    WAIT_BEFORE_MENTION("&ePlease wait %time% before using this mention again !");

    private final String content;
    private TranslationsManager manager;

    UMEN(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(UMEN::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationsManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationsManager getManager() {
        return manager;
    }


    @Override
    public String toString() {
        return this.get().translate();
    }
}

